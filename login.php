<?php 
session_start();
require_once 'instellingen.php';
if (empty($_POST["email"]) || empty($_POST["wachtwoord"])) {
    header("Location:index.php?inlogleeg=true");
    exit;
}
?>

<!--verbinden met database-->
<?php
$pdo = instellingen::getPDO();
?>

<!--database checken met inloggegevens-->
<?php
//als databaseverbinding fout gaat, stuur naar errorpagina
if ($pdo->bIsSuccess === false) {
    header("Location:errorpagina.php");
    exit;
}

$pdo = $pdo->uReturnData;

$email = $_POST["email"];

//haal wachtwoord van account uit database
$query1 = "Select wachtwoord
                From account
                Where email = ?";
$stmt = $pdo->prepare($query1);
$stmt->execute(array($email));
$row = $stmt->fetch();

//zet wachtwoorden in variabele
$hashedww = $row["wachtwoord"];
$ingevuldww = $_POST["wachtwoord"];

//vergelijk db wachtwoord met ingevuld wachtwoord, als het fout is krijgt de gebruiker een melding
if (password_verify($ingevuldww, $hashedww) === false) {
    header("Location:index.php?inlogfout=true");
    exit;
}

//gegevens ophalen om in de sessie te zetten
$query2 = "Select PersoneelsID, Voornaam, Achternaam, Rol
           From personeelsgegevens
           Where personeelsID = (Select personeelsID 
                                 From account
                                 Where email = ?)";

$stmt1 = $pdo->prepare($query2);
$stmt1->execute(array($email));
$row = $stmt1->fetch();

//zet alle gegevens in sessie
$_SESSION["rol"] = $row["Rol"];
$_SESSION["personeelsID"] = $row["PersoneelsID"];
$_SESSION["voornaam"] = $row["Voornaam"];
$_SESSION["email"] = $email;
$_SESSION["achternaam"] = $row["Achternaam"];
$pdo = NULL;

header("Location:1Rooster/roosterPagina.php");
exit;
?>