<html lang="nl">
    <head>
        <title>Log In - Urensysteem De Klaampe</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
              integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    </head>
    <body>
        <br><br><br><br><br>
        <div class="container text-center">
            <div class="container text-center col-10 col-lg-4">
                <img src="Images/kulturhuslogin.png" class="img-fluid" alt="Kulturhus De Klaampe"><br><br>
            </div>
            <div class="container text-center col-8">
                <form action="../urensysteem/login.php" method="post">
                    <div class="form-group">
                        <input type="email" name="email" placeholder="E-mailadres" size="20" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" name="wachtwoord" placeholder="Wachtwoord" size="20" class="form-control">
                    </div>
                    <div>
                        <input class="btn btn-outline-dark" type="submit" name="login" value="Log In">
                    </div>
                </form>
            </div>
            <div class="container text-center col-8">
                <br>
                <?php
                if (isset($_GET["uitgelogd"])) {
                    echo "U bent uitgelogd.";
                }

                if (isset($_GET["inlogfout"])) {
                    echo "Gebruikersnaam of wachtwoord is fout";
                }

                if (isset($_GET["inlogleeg"])) {
                    echo "Vul de velden alstublieft in.";
                }
                ?>
            </div>
        </div>

<?php
//bootstrap scripts
include './include/scripts.php';
?>
    </body>
</html>