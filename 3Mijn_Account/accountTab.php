<?php
//variabelen maken van sessie data zodat ik ze simpeler kan opvragen
$fullname = $_SESSION["voornaam"] . " " . $_SESSION["achternaam"];
$id = $_SESSION["personeelsID"];
$rol = $_SESSION["rol"];
$email = $_SESSION["email"];
?>

<!--Account tab-->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-xs-offset-0 col-sm-offset-0 toppad">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php
                        echo $fullname;
                        ?>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <table class="table table-user-information">
                            <?php
                            echo '
                            <tbody>
                            <tr>
                                <td>Personeelscode</td>
                                <td>' . $id . '</td>
                            </tr>
                            <tr>
                                <td>Rol</td>
                                <td>' . ucfirst($rol) . '</td>
                            </tr>
                            <tr>
                                <td>E-mailadres</td>
                                <td><a href=mailto:' . $email . '>' . $email . '</a></td>
                            </tr>
                            </tbody>';
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>