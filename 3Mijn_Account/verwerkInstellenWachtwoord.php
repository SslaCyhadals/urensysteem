<?php
require_once '../include/navbar.php';
?>

<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>

<?php
//Ga naar errorpagina.php wanneer per ongeluk op deze pagina is gekomen.
if ((empty($_POST["submit"]) && empty($_POST["check"])) || empty(trim($_SESSION["token"]))) {
    header('location:login.php');
    exit;

    //Maak een variabele onvolledig wanneer niet alle wachtwoordvelden zijn ingevuld.
}

$pdo = instellingen::getPDO();
if (instellingen::getPDO()->bIsSuccess === false) {
    header('location:errorpagina.php');
    exit;
}

$pdo = $pdo->uReturnData;

if (isset($_POST["checkGebruikersnaam"])) {

    //Check of de gebruikersnaam is ingevuld.

    $testGebruikersnaam = $_POST["checkGebruikersnaam"];
    $testToken = $_SESSION["token"];

    //Check of de gebruikersnaam bij het token past.
    $sql = "SELECT gebruikersnaam FROM wachtwoordReset WHERE gebruikersnaam = ? AND wachtwoordToken = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array($testGebruikersnaam, $testToken));


    //Ga naar errorpagina.php wanneer de gebruikersnaam er niet bij klopt.
    if ($_POST["checkGebruikersnaam"] === $stmt->fetch()["gebruikersnaam"]) {
        $_SESSION["gebruikersnaam"] = $_POST["checkGebruikersnaam"];
        header('location:instellenWachtwoord.php');
        exit;
    }
} else if (!empty(trim($_POST["wachtwoord"])) && !empty(trim($_POST["wachtwoord2"]))) {

    //Maak een variabele verschillend wanneer de wachtwoorden niet overeenkomen.
    if ($_POST["wachtwoord"] != $_POST["wachtwoord2"]) {
        $_SESSION["verschillend"] = true;
        header('location:instellenWachtwoord.php');
        exit;
    } else {
        //Verander het wachtwoord van het account naar het nieuwe wachtwoord.
        $gebruikersnaam = $_SESSION["gebruikersnaam"];
        $wachtwoord = password_hash($_POST["wachtwoord"], PASSWORD_BCRYPT);


        $sql = "UPDATE account SET wachtwoord = ? WHERE gebruikersnaam = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(array($wachtwoord, $gebruikersnaam));

        //Verwijder de token waarmee het wachtwoord veranderd kon worden.
        $sql = "DELETE FROM wachtwoordReset WHERE gebruikersnaam = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(array($gebruikersnaam));

        $pdo = null;

        $_SESSION["aangepast"] = true;


        header('location:instellenWachtwoord.php');
        exit;
    }
} else {
    $_SESSION["onvolledig"] = true;
    header('location:instellenWachtwoord.php');
    exit;
}
?>

</body>
</html>
