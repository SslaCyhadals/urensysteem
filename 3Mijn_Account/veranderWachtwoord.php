<div class="container-fluid">
    <!--verander wachtwoord-->
    <!--knop, als op de knop wordt gedrukt voert de verwerkToken.php uit zonder de pagina te verlaten-->
    <a class="btn btn-outline-dark" href="../verwerkToken.php?token=ZvrC3USdTBEQ8llqhvIBkCxOzeJ89OaG">Verander mijn
        wachtwoord</a>

</div>

<?php
$onvolledigtext = false;
//instellenWachtwoord.php stuurt een variabele mee wanneer niet alle velden zijn ingevuld.
// Hier wordt de modal automatisch geopend zodat de gebruiker opnieuw kan proberen.
if (isset($_SESSION["onvolledig"])) {
    //modal popt up wanneer de pagina laadt
    echo "<script> $('#wachtwoordmodal').modal('show')</script>";
    $onvolledigtext = true;
    $_SESSION["onvolledig"] = null;
}
?>

<!--verander wachtwoord modal-->
<div class="modal fade" id="wachtwoordmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Wachtwoord veranderen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                //                if (!isset($_SESSION["token"])) {
                //                    header('location:../errorpagina.php');
                //                    exit;
                //                }

                //verwerkInstellenWachtwoord.php stuurt een variabele mee wanneer het wachtwoord is aangepast.
                if (isset($_SESSION["aangepast"])) {
                    print("Uw wachtwoord is aangepast. U kunt nu inloggen.");
                } else {
                    if (isset($_SESSION["checkGebruikersnaam"])) {
                        $gebruikersnaam = $_SESSION["gebruikersnaam"];
                        ?>
                        <form method="post" action="verwerkInstellenWachtwoord.php">
                            Gebruikersnaam: <input title="gebruikersnaam" type="text" disabled=""
                                                   value="<?php print($gebruikersnaam); ?>"><br>
                            Wachtwoord: <input title="wachtwoord" type="text" name="wachtwoord"><br>
                            Herhaal wachtwoord<input title="herhaalww" type="text" name="wachtwoord2"><br>
                            <input type="submit" name="submit" value="Verander Wachtwoord">
                        </form>

                        <?php
                    } else {
                        ?>

                        <form method="post" action="verwerkInstellenWachtwoord.php">
                            Gebruikersnaam: <input title="gebruikersnaamL" type="text" name="gebruikersnaamL"><br>
                            <input type="submit" name="check" value="Log in">
                        </form>

                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>