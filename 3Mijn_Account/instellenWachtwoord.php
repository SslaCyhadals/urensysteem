<?php
session_start();
?>
<!doctype html>
<html lang="nl">
<head>
    <title>Wachtwoord Instellen - Urensysteem De Klaampe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<?php
//navbar
include '../include/navbar.php';
?>
<br>

<?php
if (empty(trim($_SESSION["token"]))) {
    header('location:../errorpagina.php');
    exit;
}


//verwerkInstellenWachtwoord.php stuurt een variabele mee wanneer het wachtwoord is aangepast.
if (isset($_SESSION["aangepast"])) {
    print("Uw wachtwoord is aangepast. U kunt nu inloggen.");
} else {

    if (isset($_SESSION["gebruikersnaam"])) {
        $gebruikersnaam = $_SESSION["gebruikersnaam"];
        ?>
        <form method="post" action="verwerkInstellenWachtwoord.php">
            Gebruikersnaam: <input title="gebruikersnaam" type="text" disabled=""
                                   value="<?php print($gebruikersnaam); ?>"><br>
            Wachtwoord: <input title="wachtwoord" type="text" name="wachtwoord"><br>
            Herhaal wachtwoord<input title="herhaalww" type="text" name="wachtwoord2"><br>
            <input type="submit" name="submit" value="Verander Wachtwoord">
        </form>

        <?php
    } else {
        ?>

        <form method="post" action="verwerkInstellenWachtwoord.php">
            Gebruikersnaam: <input title="checkww" type="text" name="checkGebruikersnaam"><br>
            <input type="submit" name="check" value="Log in">
        </form>

        <?php
    }
}
?>

<?php
//bootstrap scripts
include '../include/scripts.php';
?>
</body>
</html>
