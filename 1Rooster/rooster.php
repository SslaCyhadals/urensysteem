<?php
//maken fillWeek functie
function fillWeek($ts)
{
    $week = array();
    $date = $ts;
    if (date('D', mktime()) != 'Sun') {
        $date = strtotime("Last Sunday", intval($date));
    }
    for ($i = 0; $i < 7; $i++) {
        array_push($week, $date);
        $date = strtotime("Next Day", $date);
    }
    return $week;
}


//database verbinding maken
$pdo = instellingen::getPDO();

//als de database verbinding niet werkt, wordt de gebruiker naar de errorpagina gestuurd
if ($pdo->bIsSuccess === false) {
    header('location:errorpagina.php');
    exit;
}

if (empty($_GET["d"])) {
    $d = mktime();
} else {
    $d = $_GET["d"];
}
$week = fillWeek($d);

$urenWeek = 0;

$zondag = date('Y-m-d', $week[0]);
$zaterdag = date('Y-m-d', $week[6]);

$pdo = $pdo->uReturnData;
$sql = "SELECT *, TIMEDIFF(eindtijd, begintijd) aantalUren FROM rooster
                    WHERE datum BETWEEN ? AND ? 
                    AND personeelsID = ?
                    ORDER BY datum ASC";
$stmt = $pdo->prepare($sql);
$stmt->execute(array($zondag, $zaterdag, $_SESSION["personeelsID"]));

$weekdagen = array('Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag');
$row = $stmt->fetch();
$urenVandaag = intval($row["aantalUren"]);
?>

    <div class="container-fluid">
        <div class="container-fluid">
            <?php
            if (isset($_GET['algevraagd'])) {
                if ($_GET['algevraagd']) {
                    echo 'Deze dag is al vrij gevraagd!';
                }
            }
            if (isset($_GET['verstuurd'])) {
                if ($_GET['verstuurd']) {
                    echo 'Aanvraag is verstuurd!';
                }
            }

            ?>
        </div>
        <div class="container-fluid margin">
            <div class="row">
                <!--Vorige week-->
                <div class="col-sm">
                    <div class="float-left text-center">
                        <a class="btn btm-sm btn-outline-dark" href="roosterPagina.php?d=<?= $d - 604800 ?>"
                           role="button">Vorige
                            week</a>
                    </div>
                </div>
                <?php
                if ($_SESSION['rol'] == 'vrijwilliger') {
                    ?>
                    <div class="col-sm">
                        <!--Vrij vragen-->
                        <div class="text-center">
                            <button type="button" class="btn btm-sm btn-outline-dark" data-toggle="modal"
                                    data-target=".bd-example-modal-sm">Vrij vragen
                            </button>
                        </div>
                        <!--Vrij vragen modal-->
                        <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog"
                             aria-labelledby="mySmallModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="row">
                                        Welke dag wil je vrij?
                                    </div>
                                    <form method="post" action="verwerkVrijVragen.php">
                                        <div class="row">
                                            <input class="form-control" type="date" data-type="date" id="date"
                                                   name="date"
                                                   min="<?= date('Y-m-d', mktime()); ?>"
                                                   max="<?= date('Y-m-d', strtotime('+ 1 year', mktime())) ?>"
                                                   placeholder="dd-mm-yyyy"
                                                   value="02-01-2018" required="" aria-required="true">
                                        </div>
                                        <div class="row">
                                            <input class="btn btn-outline-dark" type="submit">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <!--Volgende week-->
                <div class="col-sm">
                    <div class="float-right">
                        <a class="btn btm-sm btn-outline-dark" href="roosterPagina.php?d=<?= $d + 604800 ?>"
                           role="button">Volgende
                            week</a>
                    </div>
                </div>
            </div>
        </div>
        <br>

        <div class="container-fluid">
            <?php
            if ($_SESSION['rol'] == 'directie') {
                include 'roosterDirectie.php';
            }
            ?>
        </div>

        <div class="container-fluid margin">
            <div class="row">
                <!--Uren vandaag-->
                <div class='col-md daguren '>
                    Aantal uren vandaag: <?= $urenVandaag ?>
                </div>
                &nbsp
                <!--Uren deze week-->
                <div class='col-md weekuren'>
                    Aantal uren deze Week: <?= $urenWeek ?>
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid margin">
            <div class="row">
                <!--weekdagen blokjes-->
                <?php for ($i = 0;
                $i < count($week);
                $i++) { ?>
                <div class='col-sm dag' style='
                <?php
                if (date('Y-m-d', $week[$i]) === date('Y-m-d', mktime())) {
                    print("background: #f8991d");
                    $urenVandaag = intval($row["aantalUren"]);
                } ?>
                        '>
                    <?= $weekdagen[$i] . "<br>" . date('d-M-Y', $week[$i]) ?>

                    <?php
                    if (date('Y-m-d', $week[$i]) === $row["datum"]) {
                        print("<br><br>" . $row["opdracht"]
                            . "<br><br>Van: " . $row["begintijd"]
                            . "<br>Tot: " . $row["eindtijd"]);
                        $urenWeek += intval($row["aantalUren"]);
                        $row = $stmt->fetch();
                    } else {
                        print("<br><br>Geen werk vandaag");
                    }
                    print("</div>&nbsp");
                    }
                    ?>
                </div>

            </div>
        </div>

    </div>


<?php
$pdo = NULL;
?>