<!--Als er zojuist is ingelogd, wordt een modal geopend waar wordt gevraagd of de gebruiker werkt of niet.-->
<?php
if (isset($_SESSION["login"])) { ?>

    <script>
        //modal popt up wanneer de pagina laadt
        $('#werkmodal').modal('show');
    </script>

    <!-- Modal html -->
    <div id="werkmodal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Wil je inklokken voor werk?</h4>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-dark" role="button" data-dismiss="modal">Nee</button>
                    <button class="btn btn-outline-dark" role="button" id="werkmodalja" onclick="werk()">Ja</button>
                </div>
            </div>
        </div>
    </div>

    <!--Ja knop-->
    <script>
        werk()
        {
            //tijd wanneer werk start wordt geregistreerd
            $('button#werkmodalja').on("click", function (event) {
                // form
                event.preventDefault();
                $('#werkmodal').modal('hide');
            }
    </script>

<?php } ?>