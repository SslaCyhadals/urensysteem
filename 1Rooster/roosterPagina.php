<html lang="nl">
<head>
    <title>Rooster - Urensysteem De Klaampe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <!--link rooster.css voor rooster.php-->
    <link href="rooster.css" rel="stylesheet" type="text/css" media="all">

</head>
<body>
<?php
//navbar
include '../include/navbar.php';
?>
<br>


<?php
//wanneer zojuist is ingelogd, popt een modal up waar wordt gevraagd of de persoon wil inklokken voor werk of niet
include 'checkWerk.php';

//rooster code
include 'rooster.php';
?>


<?php
//bootstrap scripts
include '../include/scripts.php';
?>
</body>
</html>