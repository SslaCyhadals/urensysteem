<!-- The Modal -->
<div id="maandagModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content" style='width:50%;'>
        <span class="close">&times;</span>
        <form method='get' action='aanmakenRooster.php'>
            <input type="text" value='<?= date('d-m-Y', $week[0]); ?>' name='rDate'><br>
            Taak: <input type="text" name='taak'><br>
            Van: <input type="time" name='van' style='margin:0px 5px;'><br>
            Tot: <input type="time" name="tot" style='margin:0px 10px;'><br><br>
            Wie wilt u dat de taak uitvoert?
            <?php
            foreach ($vrijwilligers[date('Y-m-d', $week[0])] AS $vrijwilliger) {
                if ($vrijwilliger === "") {
                    print("Geen vrijwilligers beschikbaar");
                } else {
                    ?>
                    <br><input type="checkbox" name='vrijwilligers[]' value='<?= $vrijwilliger ?>'><?= $vrijwilliger ?></input>
                    <?php
                }
            }
            ?>
            <br><input type="submit" value='Plan in' name='plan'>
        </form>
    </div>
</div>

<!-- The Modal -->
<div id="dinsdagModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content" style='width:50%;'>
        <span class="close">&times;</span>
        <form method='get' action='aanmakenRooster.php'>
            <input type="text" value='<?= date('d-m-Y', $week[1]); ?>' name='rDate'><br>
            Taak: <input type="text" name='taak'><br>
            Van: <input type="time" name='van' style='margin:0px 5px;'><br>
            Tot: <input type="time" name="tot" style='margin:0px 10px;'><br><br>
            Wie wilt u dat de taak uitvoert?
            <?php
            foreach ($vrijwilligers[date('Y-m-d', $week[1])] AS $vrijwilliger) {
                if ($vrijwilliger === "") {
                    print("Geen vrijwilligers beschikbaar");
                } else {
                    ?>
                    <br><input type="checkbox" name='vrijwilligers[]' value='<?= $vrijwilliger ?>'><?= $vrijwilliger ?></input>
                    <?php
                }
            }
            ?>
            <br><input type="submit" value='Plan in' name='plan'>
        </form>
    </div>
</div>

<!-- The Modal -->
<div id="woensdagModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content" style='width:50%;'>
        <span class="close">&times;</span>
        <form method='get' action='aanmakenRooster.php'>
            <input type="text" value='<?= date('d-m-Y', $week[2]); ?>' name='rDate'><br>
            Taak: <input type="text" name='taak'><br>
            Van: <input type="time" name='van' style='margin:0px 5px;'><br>
            Tot: <input type="time" name="tot" style='margin:0px 10px;'><br><br>
            Wie wilt u dat de taak uitvoert?
            <?php
            foreach ($vrijwilligers[date('Y-m-d', $week[2])] AS $vrijwilliger) {
                if ($vrijwilliger === "") {
                    print("Geen vrijwilligers beschikbaar");
                } else {
                    ?>
                    <br><input type="checkbox" name='vrijwilligers[]' value='<?= $vrijwilliger ?>'><?= $vrijwilliger ?></input>
                    <?php
                }
            }
            ?>
            <br><input type="submit" value='Plan in' name='plan'>
        </form>
    </div>
</div>

<!-- The Modal -->
<div id="donderdagModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content" style='width:50%;'>
        <span class="close">&times;</span>
        <form method='get' action='aanmakenRooster.php'>
            <input type="text" value='<?= date('d-m-Y', $week[3]); ?>' name='rDate'><br>
            Taak: <input type="text" name='taak'><br>
            Van: <input type="time" name='van' style='margin:0px 5px;'><br>
            Tot: <input type="time" name="tot" style='margin:0px 10px;'><br><br>
            Wie wilt u dat de taak uitvoert?
            <?php
            foreach ($vrijwilligers[date('Y-m-d', $week[3])] AS $vrijwilliger) {
                if ($vrijwilliger === "") {
                    print("Geen vrijwilligers beschikbaar");
                } else {
                    ?>
                    <br><input type="checkbox" name='vrijwilligers[]' value='<?= $vrijwilliger ?>'><?= $vrijwilliger ?></input>
                    <?php
                }
            }
            ?>
            <br><input type="submit" value='Plan in' name='plan'>
        </form>
    </div>
</div>

<!-- The Modal -->
<div id="vrijdagModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content" style='width:50%;'>
        <span class="close">&times;</span>
        <form method='get' action='aanmakenRooster.php'>
            <input type="text" value='<?= date('d-m-Y', $week[4]); ?>' name='rDate'><br>
            Taak: <input type="text" name='taak'><br>
            Van: <input type="time" name='van' style='margin:0px 5px;'><br>
            Tot: <input type="time" name="tot" style='margin:0px 10px;'><br><br>
            Wie wilt u dat de taak uitvoert?
            <?php
            foreach ($vrijwilligers[date('Y-m-d', $week[4])] AS $vrijwilliger) {
                if ($vrijwilliger === "") {
                    print("Geen vrijwilligers beschikbaar");
                } else {
                    ?>
                    <br><input type="checkbox" name='vrijwilligers[]' value='<?= $vrijwilliger ?>'><?= $vrijwilliger ?></input>
                    <?php
                }
            }
            ?>
            <br><input type="submit" value='Plan in' name='plan'>
        </form>
    </div>
</div>

<!-- The Modal -->
<div id="zaterdagModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content" style='width:50%;'>
        <span class="close">&times;</span>
        <form method='get' action='aanmakenRooster.php'>
            <input type="text" value='<?= date('d-m-Y', $week[5]); ?>' name='rDate'><br>
            Taak: <input type="text" name='taak'><br>
            Van: <input type="time" name='van' style='margin:0px 5px;'><br>
            Tot: <input type="time" name="tot" style='margin:0px 10px;'><br><br>
            Wie wilt u dat de taak uitvoert?
            <?php
            foreach ($vrijwilligers[date('Y-m-d', $week[5])] AS $vrijwilliger) {
                if ($vrijwilliger === "") {
                    print("Geen vrijwilligers beschikbaar");
                } else {
                    ?>
                    <br><input type="checkbox" name='vrijwilligers[]' value='<?= $vrijwilliger ?>'><?= $vrijwilliger ?></input>
                    <?php
                }
            }
            ?>
            <br><input type="submit" value='Plan in' name='plan'>
        </form>
    </div>
</div>

<!--zondag aanmaken rooster modal-->
<div id="zondagModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content" style='width:50%;'>
        <span class="close">&times;</span>
        <form method='get' action='aanmakenRooster.php'>
            <input type="text" value='<?= date('d-m-Y', $week[6]); ?>' name='rDate'><br>
            Taak: <input type="text" name='taak'><br>
            Van: <input type="time" name='van' style='margin:0px 5px;'><br>
            Tot: <input type="time" name="tot" style='margin:0px 10px;'><br><br>
            Wie wilt u dat de taak uitvoert?
            <?php
            foreach ($vrijwilligers[date('Y-m-d', $week[6])] AS $vrijwilliger) {
                if ($vrijwilliger === "") {
                    print("Geen vrijwilligers beschikbaar");
                } else {
                    ?>
                    <br><input type="checkbox" name='vrijwilligers[]' value='<?= $vrijwilliger ?>'><?= $vrijwilliger ?></input>
                    <?php
                }
            }
            ?>
            <br><input type="submit" value='Plan in' name='plan'>
        </form>
    </div>
</div>