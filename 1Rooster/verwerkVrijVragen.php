<?php

$pdo = instellingen::getPDO();

if ($pdo->bIsSuccess === false) {
    header('location:../errorpagina.php');
    exit;
}

if (empty($_POST["date"])) {
    header('location:roosterpagina.php');
    exit;
}

$pdo = $pdo->uReturnData;
$sql = "INSERT INTO nietBeschikbareDagen(datum, personeelsID)
                VALUES(?,?)";
$stmt = $pdo->prepare($sql);
$stmt->execute(array($_POST["date"], $_SESSION["personeelsID"]));

catch
(Exception $e){
    header('roosterPagina.php?algevraagd=true');
}
header('roosterPagina.php?verstuurd=true');

?>