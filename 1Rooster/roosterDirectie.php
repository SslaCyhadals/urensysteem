<?php
//Dit is aanmakenRooster.php maar dan zonder alles wat al in rooster.php staat.

$vrijwilligers = array();
foreach ($week AS $dag) {

    $sql = "SELECT voornaam, achternaam, personeelsID
                    FROM personeelsgegevens
                    WHERE personeelsID NOT IN (SELECT personeelsID 
                                               FROM nietBeschikbareDagen
                                               WHERE datum = ?);";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(date('Y-m-d', $dag)));

    $index = 0;
    $vrijwilligers[date('Y-m-d', $dag)][0] = "";
    while ($row = $stmt->fetch()) {
        unset($vrijwilligers[date('Y-m-d', $dag)][0]);

        $voornaam = $row["voornaam"];
        $achternaam = $row["achternaam"];

        $vrijwilligers[date('Y-m-d', $dag)][$row['personeelsID']] = $voornaam . " " . $achternaam;
        $index++;
    }
}

$sql = "SELECT * 
                FROM rooster 
                WHERE datum 
                BETWEEN ? AND ?";

$stmt = $pdo->prepare($sql);
$stmt->execute(array(date('Y-m-d', $week[0]), date('Y-m-d', $week[6])));
$taken = array();
while ($row = $stmt->fetch()) {
    if (!in_array($row, $taken)) {
        array_push($taken, $row);
    }
}
?>
<div style='width:100%; text-align:center; height:20px;'>
    <?php
    if (isset($_GET['plan'])) {
        if (empty($_GET['taak']) || empty($_GET['van']) || empty($_GET['tot'])) {
            print("<span style='color:red; text-align:center;'>Niet alle velden zijn ingevuld!</span");
        } else if (empty($_GET['vrijwilligers'])) {
            print("<span style='color:red; text-align:center;'>Er zijn geen vrijwilligers ingevuld!</span>");
        } else {

            $sql = "SELECT MAX(roosterID) FROM rooster";
            $stmt = $pdo->prepare($sql);
            $stmt->execute();

            $id = $stmt->fetch()["MAX(roosterID)"] + 1;

            try {
                foreach ($_GET['vrijwilligers'] AS $vrijwilliger) {
//                        print(array_search($vrijwilliger, $vrijwilligers[date('Y-m-d', strtotime($_GET['rDate']))]));

                    $sql = "INSERT INTO rooster(roosterID, datum, begintijd, eindtijd, opdracht, personeelsID)
                                VALUES(?,?,?,?,?,?)";
                    $stmt = $pdo->prepare($sql);
                    $stmt->execute(array($id, date('Y-m-d', strtotime($_GET['rDate'])), $_GET["van"], $_GET['tot'], $_GET["taak"], array_search($vrijwilliger, $vrijwilligers[date('Y-m-d', strtotime($_GET['rDate']))])));
                }
                print("<span style='color:green;'>Taak toegevoegd!</span>");
            } catch (Exception $e) {
                print("<span style='color:red;'>Taak is al toegevoegd!</span>");
            }
        }
    }
    $pdo = null;
    ?>
</div>
<div class='row' style="width:100%">
    <?php for ($i = 0; $i < count($week); $i++) { ?>
        <div class='col-sm dag' style='max-height:50px; width:100/7%;
        <?php if (date('Y-m-d', $week[$i]) === date('Y-m-d', mktime())) { ?>
                 background:orange;
                 <?php
             }
             ?>
             '>
                 <?= $weekdagen[$i] . "<br>" . date('d-M-Y', $week[$i]); ?>
        </div>
        <?php
    }
    ?>
</div>
<div class='row' style="width:100%">
    <button id="Sun" style="grid-column:1; grid-row:1; height:50px; margin-top:50px;">Taak toevoegen</button>
    <button id="Mon" style="grid-column:2; grid-row:1; height:50px; margin-top:50px;">Taak toevoegen</button>
    <button id="Tue" style="grid-column:3; grid-row:1; height:50px; margin-top:50px;">Taak toevoegen</button>
    <button id="Wed" style="grid-column:4; grid-row:1; height:50px; margin-top:50px;">Taak toevoegen</button>
    <button id="Thi" style="grid-column:5; grid-row:1; height:50px; margin-top:50px;">Taak toevoegen</button>
    <button id="Fry" style="grid-column:6; grid-row:1; height:50px; margin-top:50px;">Taak toevoegen</button>
    <button id="Sat" style="grid-column:7; grid-row:1; height:50px; margin-top:50px;">Taak toevoegen</button>
</div>

<?php
include 'roosterDirectieMaakRoosterModals.php';
?>
<script src="roosterDirectieMaakRoosterModal.js"