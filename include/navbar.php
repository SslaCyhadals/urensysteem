<?php
if (session_id() == "")
    session_start();
require_once '../instellingen.php';

//check of gebruiker is ingelogd
$session_rol = $_SESSION['rol'];
if ($session_rol == '') {
    header("Location: errorpagina.php?");
}
?>
<nav class="navbar navbar-expand-lg navbar-light bg-white">

    <div>
        <!--mobile menu knop-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!--logo-->
        <a href="../1Rooster/roosterPagina.php">
            <img src="../Images/kulturhusNavbar.PNG" class="navbar-brand" alt="Kulturhus De Klaampe">
        </a>
    </div>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!--knoppen (unordered list)-->
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="../1Rooster/roosterPagina.php">Rooster <span
                            class="sr-only">(current)</span></a>
            </li>
            <?php
            //deze knoppen zijn er alleen als de gebruiker admin of directie is
            if ($_SESSION["rol"] === 'administrator' OR $_SESSION["rol"] === 'directie') :?>
                <li class="nav-item">
                    <a class="nav-link" href="../2Instellingen/instellingenpagina.php">Instellingen</a>
                </li>
            <?php endif; ?>
            <li class="nav-item">
                <a class="nav-link" href="../3Mijn_Account/mijnaccountpagina.php">Mijn Account</a>
            </li>
        </ul>
        <span class="navbar-text">
            <?php
            //display van naam en rol
            print ("Ingelogd als " . $_SESSION["voornaam"] . " " . $_SESSION["achternaam"] . " (" . $_SESSION["rol"] . ")");
            ?>
        </span>
        <p>&nbsp;</p>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="btn btn-outline-dark" href="../loguit.php" role="button">Log uit</a>
            </li>
        </ul>
    </div>

</nav>