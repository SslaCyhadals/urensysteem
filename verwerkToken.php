<?php
session_start();
require_once 'instellingen.php';

//Ga naar de inlogpagina wanneer er geen token is meegestuurd
if (!isset($_GET["token"])) {
    header('location:index.php');
    exit;
}
$token = $_GET["token"];

$pdo = instellingen::getPDO();

if (instellingen::getPDO()->bIsSuccess) {
    $pdo = $pdo->uReturnData;

    //Check of het token in het database zit
    $sql = "SELECT wachtwoordToken
                    FROM wachtwoordReset
                    WHERE wachtwoordToken = ?;
                    ";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array($token));

    $pdo = null;
    //wat gebeurt hieronder? ik kom niet in de if statement
    if ($_GET["token"] = $stmt->fetch()["wachtwoordToken"]) {
        header('location:3Mijn_Account/instellenWachtwoord.php');
        exit;
    }
}
header('location:errorpagina.php');
exit;
?>