<?php
session_start();
?>
<?php require_once("./instellingen.php"); ?>
<!doctype html>
<html lang="nl">
<head>
    <title>Rooster - Urensysteem De Klaampe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<div class="container text-center">
    <h5>
        <br><br><br><br>
        Er is iets fout gegaan.<br><br>
        <?php
        //als de gebruiker niet kan inloggen en naar dit bestand wordt gestuurd, gaat de Terug knop naar index ipv. hoofdpagina.
        if ($_SESSION["inlogfout"] = true){
            session_destroy();
            echo '<a class="btn btn-outline-dark" href="index.php">Terug naar inloggen</a>';
        } else {
            echo '<a class="btn btn-outline-dark" href="<?= Instellingen::$sRootUrl ?>1Rooster/roosterpagina.php">Terug naar hoofdpagina</a>';
        }
        ?>
    </h5>
</div>


<?php
//bootstrap scripts
include './include/scripts.php';
?>
</body>
</html>