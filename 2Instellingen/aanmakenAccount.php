<div class="container-fluid">
    <button class="btn btn-outline-dark" data-toggle="modal" data-target="#accountmodal">Account aanmaken</button>
    <br><br>
</div>

<?php
$onvolledigtext = false;
//verwerkAanmakenAccount.php stuurt een variabele mee wanneer niet alle velden zijn ingevuld.
// Hier wordt de modal automatisch geopend zodat de gebruiker opnieuw kan proberen.
if (isset($_SESSION["onvolledig"])) {
    //modal popt up wanneer de pagina laadt
    echo "<script> $('#accountmodal').modal('show')</script>";
    $onvolledigtext = true;
    $_SESSION["onvolledig"] = null;
}
?>

<!--account aanmaken modal-->
<div class="modal fade" id="accountmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Account maken</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="verwerkAanmakenAccount.php">
                    <div>
                        Voornaam<br>
                        <input type="text" name="voornaam" placeholder="Jan" class="form-control">
                    </div>
                    <div>
                        Achternaam<br>
                        <input type="text" name="achternaam" placeholder="Piet" class="form-control">
                    </div>
                    <div>
                        Emailadres<br>
                        <input type="email" name="email" placeholder="testEmail@test.nl">
                    </div>
                    <div>
                        Rol<br>
                        <select name="rol">
                            <option value="kies">Kies rol</option>

                            <option value="vrijwilliger">Vrijwilliger</option>
                            <option value="directielid">Directielid</option>
                            <option value="administrator">Administrator</option>
                        </select>
                    </div>

                    <?php
                    //nadat de modal automatisch opent, wordt dit geprint.
                    if ($onvolledigtext == true) {
                        print("<p style=\"color:red\">Niet alle velden zijn ingevuld.</p>");
                    }
                    ?>

                    <br>
                    <input class="btn btn-outline-dark" type="submit" name="aanmaken" value="Account aanmaken">

                </form>
            </div>
        </div>
    </div>
</div>