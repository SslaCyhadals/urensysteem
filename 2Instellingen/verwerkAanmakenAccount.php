<!doctype html>
<html lang="nl">
<head>
    <title>Rooster - Urensysteem De Klaampe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<?php
//navbar
include '../include/navbar.php';
?>

<div class="container text-center">
    <?php
    require_once '../instellingen.php';

    //    //Ga terug naar aanmakenAccount.php wanneer iemand daar niet op aanmaken heeft geklikt, maar toch bij deze pagina is gekomen.
    //    if (empty($_GET["aanmaken"])) {
    //        header('location:login.php');
    //        exit;
    //    }
    //    //Ga terug naar aanmakenAccount.php wanneer niet alle velden zijn ingevuld op die pagina. Stuur een variable onvolledig mee.
    //    if (empty(trim($_GET['personeelsID'])) || empty(trim($_GET['voornaam'])) || empty(trim($_GET['achternaam'])) || empty(trim($_GET['email'])) || $_GET['rol'] === "kies") {
    //        $_SESSION["onvolledig"] = true;
    //        header('location:instellingenpagina.php');
    //        exit;
    //    }

    $email = $_POST["email"];

    uploadToDatabase($email);
    stuurEmail($email);
    print("Account toegevoegd<br>");

    function uploadToDatabase($email)
    {
        $voornaam = $_POST["voornaam"];
        $achternaam = $_POST["achternaam"];
        $rol = $_POST["rol"];
        $gebruikersnaam = $voornaam . $achternaam;

        $pdo = instellingen::getPDO();

        if ($pdo->bIsSuccess) {

            $pdo = $pdo->uReturnData;

            uploadPersoonsgegevens($voornaam, $achternaam, $rol, $pdo);

            uploadAccount($gebruikersnaam, $email, $pdo);

            uploadToken($gebruikersnaam, $pdo);

            $pdo = null;
        } else {
            header('location:../errorpagina.php');
        }
    }

    //Upload de persoonsgegevens naar het database.
    function uploadPersoonsgegevens($voornaam, $achternaam, $rol, $pdo)
    {
        $sql = "INSERT INTO personeelsgegevens(voornaam, achternaam, rol)
                    VALUES(?,?,?)";

        $stmt = $pdo->prepare($sql);
        //hier word ik ineens naar 3Instellingen/index.php? gestuurd???
        $stmt->execute(array($voornaam, $achternaam, $rol));
    }

    //Upload de gegevens die nodig zijn voor het account naar het database.
    function uploadAccount($gebruikersnaam, $email, $pdo)
    {
        $sql = "INSERT INTO account(gebruikersnaam, `E-mail`)
                    VALUES(?,?)";

        $stmt = $pdo->prepare($sql);
        $stmt->execute(array($gebruikersnaam, $email));
    }

    //Upload de gegevens die nodig zijn om een wachtwoord bij het account te kunnen maken.
    function uploadToken($gebruikersnaam, $pdo)
    {
        $sql = "INSERT INTO wachtwoordReset(wachtwoordToken, gebruikersnaam)
                    VALUES(?,?)";

        $token = maakWachtwoordToken();

        $stmt = $pdo->prepare($sql);
        $stmt->execute(array($token, $gebruikersnaam));
    }

    //Maak een willekeurige token van 32 characters.
    function maakWachtwoordToken()
    {
        $characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        $token = "";
        for ($i = 0; $i < 32; $i++) {
            $char = $characters[rand(0, strlen($characters)) - 1];
            $token .= $char;
        }
        return $token;
    }

    function stuurEmail($email)
    {
        $email = $_POST["email"];
        print("Email verstuurd!<br>");
    }

    ?>
    <br><a class="btn btn-outline-dark" href="instellingenpagina.php">Terug</a>
</div>


<?php
//bootstrap scripts
include '../include/scripts.php';
?>
</body>
</html>