<?php

// PHP versie check
if (version_compare(PHP_VERSION, "7.0.0", '<'))
    throw new Exception("Dit bestand moet worden geïmporteerd met PHP versie 7.0.0 of hoger. De huidige versie is " . PHP_VERSION);

class Instellingen
{

    public static $aDataFile = [];

    public static $sRootDir = "";
    public static $sRootUrl = "";

    public static $sMailHost = "";
    public static $iMailPort = -1;
    public static $sMailSecurity = "";
    public static $bMailHtml = false;
    public static $sMailUser = "";
    public static $sMailPass = "";
    public static $sMailFrom = "";

    public static $sHost = "";
    public static $sGebruikersnaam = "";
    public static $sWachtwoord = "";
    public static $sDatabase = "";
    public static $sPoort = "";


    /**
     * Checkt tabel integriteit en haalt instellingen op uit de database
     * @return ResultObject Een associative array met instellingen
     * @throws Exception
     * @deprecated Er zijn op dit moment geen instellingen in gebruik, alles staat nu in data.ini
     */
    public static function get() : ResultObject
    {
        // Database verbinding ophalen
        $oResult = self::getPDO();
        $dbConn = null;
        if (!$oResult->bIsSuccess)
            // Return de ResultObject met error van getPDO
            return $oResult;

        $dbConn = $oResult->uReturnData;

        // Zorg ervoor dat de instelling tabel in de database de juiste records heeft
        self::populateDatabase($dbConn);

        // Haal de instellingen uit de 'instelling' tabel
        $aResult = [];
        $oRes = $dbConn->query("SELECT k, v FROM instelling");
        foreach ($oRes as $aRow)
        {
            $aResult[$aRow["k"]] = $aRow["v"];
        }

        $dbConn = null;
        return new ResultObject(true, $aResult);
    }

    /**
     * Zorgt ervoor dat de 'instelling' tabel alle instellingen bevat
     * Gebruikt een bestaande PDO verbinding
     * @param PDO $dbConn Een bestaande verbinding
     * @deprecated Er zijn op dit moment geen instellingen in gebruik, alles staat nu in data.ini
     */
    private static function populateDatabase(PDO &$dbConn) : void
    {
        // Maak prepared statement voor het toevoegen van records
        $oStmt = $dbConn->prepare("INSERT IGNORE INTO instelling SET k = ?, v = ''");

        // Instelling records toevoegen aan de tabel
        $oStmt->execute(["mailHost"]);
        $oStmt->execute(["mailSecurity"]);
        $oStmt->execute(["mailUser"]);
        $oStmt->execute(["mailPass"]);
    }

    /**
     * Maakt verbinding met de database en geeft een ResultObject met een PDO of PDOException instantie
     * @return ResultObject Het resultaat
     */
    public static function getPDO() : ResultObject
    {
        try
        {
            // Probeer een verbinding te maken met de database
            $dbConn = new PDO("mysql:host=" . self::$sHost . ";port=" . self::$sPoort . ";dbname=" . self::$sDatabase,
                self::$sGebruikersnaam, self::$sWachtwoord);
            // Zorg ervoor dat errors van queries Exceptions maken
            $dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return new ResultObject(true, $dbConn);
        }
        catch (PDOException $e)
        {
            return new ResultObject(false, $e);
        }
    }

}


class ResultObject
{
    public $bIsSuccess;
    public $uReturnData;

    /**
     * Constructor functie voor ResultObject, maakt het mogelijk om objecten van deze klasse te maken
     * @param bool $bSuccess Of het resultaat een succes is
     * @param $uData mixed Data dat wordt doorgegeven
     */
    public function __construct(bool $bSuccess, $uData)
    {
        $this->bIsSuccess = $bSuccess;
        $this->uReturnData = $uData;
    }

    /**
     * Destructor functie voor ResultObject, zorgt ervoor dat de data goed wordt verwijderd
     * Kan niet handmatig gebruikt worden, wordt gebruikt bij 'garbage collection'
     */
    public function __destruct()
    {
        $this->uReturnData = null;
    }
}



// Ini bestand aanmaken als deze nog niet bestaat
if (!file_exists(__DIR__ . "/data.ini"))
    file_put_contents(__DIR__ . "/data.ini", <<<'EOD'
[core]
rootdir=/
rooturl=http://localhost/

[mail]
host=
port=
security=tls
html=1
user=
pass=
from=

[database]
host=localhost
user=root
pass=
db=
port=3306
EOD
    );

// Data lezen uit data.ini bestand voor de Instellingen klasse
// Wordt uitgevoerd tijdens het includen van dit bestand
Instellingen::$aDataFile = parse_ini_file(__DIR__ . "/data.ini", true);
if (array_key_exists("core", Instellingen::$aDataFile)) {
    if (array_key_exists("rootdir", Instellingen::$aDataFile["core"]))
        Instellingen::$sRootDir = Instellingen::$aDataFile["core"]["rootdir"];
    if (array_key_exists("rooturl", Instellingen::$aDataFile["core"]))
        Instellingen::$sRootUrl = Instellingen::$aDataFile["core"]["rooturl"];
}
if (array_key_exists("mail", Instellingen::$aDataFile)) {
    if (array_key_exists("host", Instellingen::$aDataFile["mail"]))
        Instellingen::$sMailHost = Instellingen::$aDataFile["mail"]["host"];
    if (array_key_exists("port", Instellingen::$aDataFile["mail"]))
        Instellingen::$iMailPort = intval(Instellingen::$aDataFile["mail"]["port"]);
    if (array_key_exists("security", Instellingen::$aDataFile["mail"]))
        Instellingen::$sMailSecurity = Instellingen::$aDataFile["mail"]["security"];
    if (array_key_exists("html", Instellingen::$aDataFile["mail"]))
        Instellingen::$bMailHtml = filter_var(Instellingen::$aDataFile["mail"]["html"], FILTER_VALIDATE_BOOLEAN);
    if (array_key_exists("user", Instellingen::$aDataFile["mail"]))
        Instellingen::$sMailUser = Instellingen::$aDataFile["mail"]["user"];
    if (array_key_exists("pass", Instellingen::$aDataFile["mail"]))
        Instellingen::$sMailPass = Instellingen::$aDataFile["mail"]["pass"];
    if (array_key_exists("from", Instellingen::$aDataFile["mail"]))
        Instellingen::$sMailFrom = Instellingen::$aDataFile["mail"]["from"];
}
if (array_key_exists("database", Instellingen::$aDataFile)) {
    if (array_key_exists("host", Instellingen::$aDataFile["database"]))
        Instellingen::$sHost = Instellingen::$aDataFile["database"]["host"];
    if (array_key_exists("user", Instellingen::$aDataFile["database"]))
        Instellingen::$sGebruikersnaam = Instellingen::$aDataFile["database"]["user"];
    if (array_key_exists("pass", Instellingen::$aDataFile["database"]))
        Instellingen::$sWachtwoord = Instellingen::$aDataFile["database"]["pass"];
    if (array_key_exists("db", Instellingen::$aDataFile["database"]))
        Instellingen::$sDatabase = Instellingen::$aDataFile["database"]["db"];
    if (array_key_exists("port", Instellingen::$aDataFile["database"]))
        Instellingen::$sPoort = Instellingen::$aDataFile["database"]["port"];
}